# Introduction to ThreeJS

This is a minimal boilerplate used during our [ThreeJS](https://threejs.org/) / WebGL workshop.

We will be using Microsoft's Visual Code which is very helpful with JavaScript autocompletions. [Go ahead and download here](https://code.visualstudio.com/).

We use node for the sake of two things:

1. It uses Budo static server with live reloading (code changes will be automatically applied)
2. By including three package, Visual Code will understand the whole library and will help us learn faster thanks to autocompletion.

Let's start by [downloading this repo](https://bitbucket.org/jocabola/introduction-to-threejs/downloads/?tab=tags).

Then open a Terminal window and go to the path where you installed (unzipped) this and run the following commands:

```
npm install
npm start
```

First command will install necessary dependencies and the second one should launch the static server and open a broswer window with a spinning red cube. If you see this, you are all set!

## Versions

We will be using different releases throughout the course. Changelog below:



#### 0.0.1 (Current)

Initial boilerplate. Introducing core concepts and the animation loop.