/*
 * global variables
 */

 var camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 1000 );
 var scene = new THREE.Scene();
 scene.add( camera );
 camera.position.z = 5;

 var renderer = new THREE.WebGLRenderer( {
     antialias: true,
     alpha: true
 } );
 renderer.setSize(window.innerWidth, window.innerHeight);

 document.body.appendChild( renderer.domElement );

/*
 * Events
 */

 window.addEventListener( "mousemove", ( event ) => {
     onMouseMove( event.clientX, event.clientY );
 }, false );

window.addEventListener("mousedown", (event) => {
    onMouseDown( event.clientX, event.clientY );
}, false);

window.addEventListener("mouseup", (event) => {
    onMouseUp(event.clientX, event.clientY);
}, false);

window.addEventListener("keydown", (event) => {
    onKeyDown(event.key.toLowerCase());
}, false);

window.addEventListener("keyup", (event) => {
    onKeyUp(event.key.toLowerCase());
}, false);

 window.addEventListener( "resize", ( event ) => {
     camera.aspect = window.innerWidth / window.innerHeight;
     camera.updateProjectionMatrix();
     renderer.setSize(window.innerWidth, window.innerHeight);
 }, false );

/*
* Render function (called on each frame)
*/

function render() {
    renderer.render(scene, camera);
}