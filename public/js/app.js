/*
 * Variables & Setup
 */

var clock = new THREE.Clock(true);

camera.position.z = 3;

/*
 * Update function (called on each frame)
 */

function update() {
    // fetch current time
    var delta = clock.getDelta();
    time = clock.elapsedTime;
}

/*
 * Interaction events
 */

 function onMouseMove( x, y ) {
    //  console.log( "Mouse Screen Coords", x, y );
    //  mouse.x = (x / window.innerWidth) * 2 - 1;
    //  mouse.y = - (y / window.innerHeight) * 2 + 1;
    //  console.log("Mouse Normalized Coords", mouse.x, mouse.y);
 }

function onMouseDown(x, y) {
    
}

function onMouseUp(x, y) {
    
}

function onKeyDown( key ) {
    
}

function onKeyUp(key) {
    
}